import * as ts from "typescript";
declare class ProjectOptions {
    readonly baseUrl: string;
    readonly outDir: string;
    private aliases;
    private paths;
    constructor(compilerOptions: ts.CompilerOptions);
    getMapping(requestedModule: string): string | null;
    private processMappings;
}
export default class AliasResolver {
    readonly srcPath: string;
    readonly outPath: string;
    readonly options: ProjectOptions;
    constructor(compilerOptions: ts.CompilerOptions);
    resolve(fileName: string, requestedModule: string): string;
}
export {};
