"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var transformer_1 = require("./transformer");
Object.defineProperty(exports, "default", { enumerable: true, get: function () { return transformer_1.default; } });
var transformer_2 = require("./transformer");
Object.defineProperty(exports, "transformerFactory", { enumerable: true, get: function () { return transformer_2.transformerFactory; } });
