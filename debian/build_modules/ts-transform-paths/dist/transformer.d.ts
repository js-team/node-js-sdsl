import * as ts from "typescript";
import { ITransformerOptions, Transformer, TransformerNode } from "./types";
export default function transformer(program?: ts.Program, options?: ITransformerOptions): Transformer;
export declare function transformerFactory<T extends TransformerNode>(context: ts.TransformationContext, options?: ITransformerOptions): ts.Transformer<T>;
