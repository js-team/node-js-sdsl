import * as ts from "typescript";
export declare function isRequireCall(node: ts.Node, checkArgumentIsStringLiteralLike: boolean): node is ts.CallExpression;
export declare function isImportCall(node: ts.Node): node is ts.CallExpression;
export declare function chainBundle<T extends ts.SourceFile | ts.Bundle>(transformSourceFile: (x: ts.SourceFile) => ts.SourceFile): (x: T) => T;
