import * as ts from "typescript";
export declare type Transformer = Required<Pick<ts.CustomTransformers, "after" | "afterDeclarations">>;
export declare type TransformerNode = ts.Bundle | ts.SourceFile;
export interface ITransformerOptions {
}
