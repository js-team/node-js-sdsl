export declare function stripWildcard(path: string): string;
export declare function replaceDoubleSlashes(filePath: string): string;
export declare function ensureTrailingPathDelimiter(searchPath: string): string;
export declare function getAlias(requestedModule: string): string;
