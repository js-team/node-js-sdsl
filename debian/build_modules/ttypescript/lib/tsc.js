"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var resolve = require("resolve");
var loadTypescript_1 = require("./loadTypescript");
var path_1 = require("path");
var vm_1 = require("vm");
var ts = (0, loadTypescript_1.loadTypeScript)('typescript', { folder: process.cwd(), forceConfigLoad: true });
var tscFileName = resolve.sync('typescript/lib/tsc', { basedir: process.cwd() });
var _a = ts.version.split(".").map(function (str) { return Number(str); }), major = _a[0], minor = _a[1];
var commandLineTsCode = fs
    .readFileSync(tscFileName, 'utf8')
    .replace(major >= 4 && minor >= 9
    ? /^[\s\S]+(\(function \(ts\) {\s+var StatisticType;[\s\S]+)$/
    : /^[\s\S]+(\(function \(ts\) \{\s+function countLines[\s\S]+)$/, '$1');
var globalCode = (fs.readFileSync(tscFileName, 'utf8').match(/^([\s\S]*?)var ts;/) || ['', ''])[1];
(0, vm_1.runInThisContext)("(function (exports, require, module, __filename, __dirname, ts) {".concat(globalCode).concat(commandLineTsCode, "\n});"), {
    filename: tscFileName,
    lineOffset: 0,
    displayErrors: true,
}).call(ts, ts, require, { exports: ts }, tscFileName, (0, path_1.dirname)(tscFileName), ts);
