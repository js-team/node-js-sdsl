"use strict";
var loadTypescript_1 = require("./loadTypescript");
var ts = (0, loadTypescript_1.loadTypeScript)('typescriptServices');
module.exports = ts;
