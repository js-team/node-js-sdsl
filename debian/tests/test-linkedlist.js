const assert1 = require('assert');
const List = require('js-sdsl').LinkList;

var l1 = new List();
var t1 = "test123";
var t2 = "test321";
var d1;

/* push data */
l1.pushBack(t1);
l1.pushBack(t2);

assert1.equal(l1.size(), 2, "length should be 2");

d1 = l1.front();
l1.popFront();

assert1.equal(l1.size(), 1, "length should be 1");

assert1.equal(d1, t1, "data pushed to list is not equal");

d1 = l1.front();
l1.popFront();

assert1.equal(l1.size(), 0, "length should be 0");

assert1.equal(d1, t2, "data pushed to list is not equal");
