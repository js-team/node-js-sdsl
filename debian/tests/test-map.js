const assert1 = require('assert');
const Map = require('js-sdsl').OrderedMap;

var m1 = new Map();
var t1 = "test123";
var t2 = "test321";
var t1data = "t123";
var t2data = "t321";


/* insert data */
m1.setElement(t1, t1data);
m1.setElement(t2, t2data);

assert1.equal(m1.getElementByKey(t1), t1data, "data is not matched");
assert1.equal(m1.getElementByKey(t2), t2data, "data is not matched");
